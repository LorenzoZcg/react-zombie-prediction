import React, { Component } from "react";


class Outcome extends Component {

  predictionDiv() {
    if (this.props.prob !== null) {
      if (this.props.prob < 0.8) {
        return (
        <div>
        <div>PREDICTION: {this.props.prob}</div>
        <div>IMMUNE</div>
        </div>
      );
      } else {
        return (
        <div>
        <div>PREDICTION: {this.props.prob}</div>
        <div>NOT IMMUNE</div>
        </div>
      );
      }

    } else {
      return (
        <div>NO RESULTS</div>
      )
    }
  }

  render() {


    return (
      <div>
        {this.predictionDiv()}
      </div>
    );
  }
}

export default Outcome;
