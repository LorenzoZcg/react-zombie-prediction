const CTR_ADR = "0x95292ae79d3bdea95aee9238321d64bd433c7148";

const ABI = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "_ipfsHash",
				"type": "string"
			}
		],
		"name": "addNewDocument",
		"outputs": [
			{
				"name": "_success",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_memberAddress",
				"type": "address"
			},
			{
				"name": "_enableAdd",
				"type": "bool"
			}
		],
		"name": "manageMember",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "checkMembership",
		"outputs": [
			{
				"name": "_isAble",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"name": "ipfsFiles",
		"outputs": [
			{
				"name": "ipfsHash",
				"type": "string"
			},
			{
				"name": "insertedAt",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "totalDocuments",
		"outputs": [
			{
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
]

export {CTR_ADR, ABI};
