pragma solidity ^ 0.4.18;
contract Zpp {

    uint public totalDocuments;
    address owner;

    mapping(address => bool) public ableToAdd;

    constructor() {
        owner = msg.sender;
        ableToAdd[owner] = true;
    }

    struct IPFSFile {
        string ipfsHash;
        uint insertedAt;
    }

    IPFSFile[] public ipfsFiles;

    function checkMembership() public view returns(bool _isAble) {
        return ableToAdd[msg.sender];
    }

    function addNewDocument(string _ipfsHash)
    isAbleToAdd()
    public {
        ipfsFiles.push(IPFSFile(_ipfsHash, now));
        totalDocuments++;
    }

    function manageMember(address _memberAddress, bool _enableAdd)
    isOwner()
    public {
        ableToAdd[_memberAddress] = _enableAdd;
    }

    modifier isAbleToAdd() {
        require(ableToAdd[msg.sender] == true);
        _;
    }

    modifier isOwner() {
        require(msg.sender == owner);
        _;
    }
}
