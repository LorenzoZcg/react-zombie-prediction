import math from 'mathjs';

class Regression {

 calculate(matrix, firstValutation, secondValutation) {
  // Part 0: Preparation
  if (firstValutation === 0) {
    firstValutation = 2;
  }
  if (secondValutation === 0) {
    secondValutation = 2;
  }

  let X = math.eval('matrix[:, 1:2]', {
    matrix,
  });
  let y = math.eval('matrix[:, 3]', {
    matrix,
  });

  let m = y.length;
  let n = X[0].length;

  // Part 1: Cost Function and Gradient
  console.log('Cost Function and Gradient ...\n');

  // Add Intercept Term
  X = math.concat(math.ones([m, 1]).valueOf(), X);

  let theta = Array(n + 1).fill().map(() => [0]);
  let { cost: untrainedThetaCost, grad } = this.costFunction(theta, X, y);

  console.log('cost: ', untrainedThetaCost);
  console.log('\n');
  console.log('grad: ', grad);
  console.log('\n');

  // Part 2: Gradient Descent (without feature scaling)
  console.log('Gradient Descent ...\n');

  const ALPHA = 0.001;
  const ITERATIONS = 1000;

  theta = [[-25], [0], [0]];
  theta = this.gradientDescent(X, y, theta, ALPHA, ITERATIONS);

  const { cost: trainedThetaCost } = this.costFunction(theta, X, y)

  console.log('theta: ', theta);
  console.log('\n');
  console.log('cost: ', trainedThetaCost);
  console.log('\n');

  // Part 3: Predict admission of a student with exam scores 45 and 85
  console.log('Part 3: Immune Prediction ...\n');

  let patientVector = [1, firstValutation, secondValutation];
  let prob = this.sigmoid(math.eval('patientVector * theta', {
    patientVector,
    theta,
  }));

  console.log('Predicted zombification: ', prob);
  return prob;

}

sigmoid(z) {
  let g = math.eval(`1 ./ (1 + e.^-z)`, {
    z,
  });

  return g;
}

costFunction(theta, X, y) {

  const m = y.length;

  let h = this.sigmoid(math.eval(`X * theta`, {
    X,
    theta,
  }));

  const cost = math.eval(`(1 / m) * (-y' * log(h) - (1 - y)' * log(1 - h))`, {
    h,
    y,
    m,
  });

  const grad = math.eval(`(1 / m) * (h - y)' * X`, {
    h,
    y,
    m,
    X,
  });

  return { cost, grad };
}

gradientDescent(X, y, theta, ALPHA, ITERATIONS) {
  const m = y.length;

  for (let i = 0; i < ITERATIONS; i++) {
    let h = this.sigmoid(math.eval(`X * theta`, {
      X,
      theta,
    }));

    theta = math.eval(`theta - ALPHA / m * ((h - y)' * X)'`, {
      theta,
      ALPHA,
      m,
      X,
      y,
      h,
    });
  }

  return theta;
}

predict(theta, X) {
  let p = this.sigmoid(math.eval(`X * theta`, {
    X,
    theta,
  }));

  return p;
}
}

export default Regression;
