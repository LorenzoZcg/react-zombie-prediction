import React, { Component } from "react";
import SimpleStorageContract from "../build/contracts/SimpleStorage.json";
import getWeb3 from "./utils/getWeb3";
import Regression from "./utils/regression";
import Outcome from "./components/Outcome";
import { ABI, CTR_ADR } from "./utils/abi";
import request from "request";
import ipfs from "./ipfs";
import "./css/oswald.css";
import "./css/open-sans.css";
import "./css/pure-min.css";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      account: null,
      filesCount: null,
      metamaskStatus: null,
      contractInstance: null,
      authorized: 'NOT AUTHORIZED',
      ipfsHash: "",
      ipfsFilesArr: [],
      selectedDatasetIndex: null,
      web3: null,
      buffer: null,
      regression: new Regression(),
      firstValutation: 0,
      secondValutation: 0,
      firstValutationImg: Math.floor(Math.random() * 12),
      secondValutationImg: Math.floor(Math.random() * 12),
      prob: null
    };

    this.handleChecking = this.handleChecking.bind(this);
    this.handleOptionChange = this.handleOptionChange.bind(this);
    // this.anotherValue = this.anotherValue.bind(this);
    this.startCalculation = this.startCalculation.bind(this);
    this.getIpfsFiles = this.getIpfsFiles.bind(this);
    this.captureFile = this.captureFile.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  handleOptionChange = changeEvent => {
    console.log("selected dataset index: ", changeEvent.target.value);
    this.setState({
      selectedDatasetIndex: changeEvent.target.value
    });
  };

  handleChecking = valutationNumber => e => {
    if (e.target.checked) {
      console.log(valutationNumber);
      e.target.removeAttribute("checked");
      e.target.parentNode.style.textDecoration = "";
      console.log("checked valutation: " + valutationNumber);
      if (valutationNumber === 1) {
        this.setState({ firstValutation: this.state.firstValutation + 2 });
      } else if (valutationNumber === 2) {
        this.setState({ secondValutation: this.state.secondValutation + 2 });
      }
    } else {
      e.target.setAttribute("checked", true);
      e.target.parentNode.style.textDecoration = "line-through";
      console.log("unchecked remove valutation: " + valutationNumber);
      console.log(valutationNumber);

      if (valutationNumber === 1) {
        this.setState({ firstValutation: this.state.firstValutation - 2 });
      } else if (valutationNumber === 2) {
        this.setState({ secondValutation: this.state.secondValutation - 2 });
      }
    }
  };

  componentWillMount() {
    getWeb3
      .then(results => {
        console.log(results);
        this.setState({
          web3: results.web3
        });
        this.state.web3.version.getNetwork((err, netId) => {
          if (netId !== "4") {
            this.setState({
              metamaskStatus: "Connect MetaMask to Rinkeby"
            });
          }
        });
        this.getIpfsFiles();
      })
      .catch(e => {
        console.error(e);
        console.log("Error finding web3.");
      });
  }

  captureFile(event) {
    event.preventDefault();
    const file = event.target.files[0];
    const reader = new window.FileReader();
    reader.readAsArrayBuffer(file);
    reader.onloadend = () => {
      this.setState({ buffer: Buffer(reader.result) });
      console.log("buffer", this.state.buffer);
    };
  }

  onSubmit(event) {
    event.preventDefault();
    ipfs.files.add(this.state.buffer, (error, result) => {
      if (error) {
        console.error(error);
        return;
      }
      this.state.contractInstance.addNewDocument(
        result[0].hash,
        { from: this.state.account },
        (err, r) => {
          if (!err) return this.setState({ ipfsHash: result[0].hash });
          console.log("ifpsHash", this.state.ipfsHash);
        }
      );
    });
  }

  getIpfsFiles() {
    let promiseArray = [];
    console.log(`ABI IS ${ABI}`);
    this.setState({
      contractInstance: this.state.web3.eth.contract(ABI).at(CTR_ADR)
    });
    this.state.web3.eth.getAccounts((error, accounts) => {
      if (accounts.length === 0) {
        this.setState({
          title: "Connect MetaMask to Rinkeby"
        });
      }
      this.setState({ account: accounts[0] });
      this.state.contractInstance.checkMembership({ from: accounts[0] },
      (error, res) => {
        console.log('authorized: ', res);
        if (res) {
          this.setState({
            authorized: 'AUTHORIZED MEMBER'
          })
        }
        else {
          this.setState({
            authorized: 'NOT AUTHORIZED'
          })
        }
      })
      this.state.contractInstance.totalDocuments(
        { from: accounts[0] },
        (error, results) => {
          console.log("take number of: " + results);
          if (results > 0) {
            for (let i = 0; i < results; i++) {
              console.log("index at: ", i);
              promiseArray.push(
                this.promiseChain(this.state.contractInstance, i)
              );
            }
            Promise.all(promiseArray).then(val => {
              this.setState({
                selectedDatasetIndex: 0
              });
            });
          }
        }
      );
    });
  }

  promiseChain(contractInstance, element) {
    return new Promise((resolve, reject) => {
      contractInstance.ipfsFiles(element, (error, ipfsFilesResponse) => {
        console.log(element);
        if (error) {
          console.log("error on blockchain");
          reject();
        }
        this.setState(prevState => ({
          ipfsFilesArr: [
            ...prevState.ipfsFilesArr,
            {
              ipfsHash: ipfsFilesResponse[0],
              createdAtDate: new Date(ipfsFilesResponse[1].c["0"] * 1000)
            }
          ]
        }));
        resolve();
      });
    });
  }

  checkNet() {
    if (this.state.web3 != null) {
      this.state.web3.version.getNetwork((err, netId) => {
        if (netId !== 4) {
          return (
            <div className="">
              <b>You are not on Rinkeby</b>
            </div>
          );
        }
      });
    } else {
      console.log("web 3 not injected");
      return (
        <div className="">
          <b>Not connected, check Metamask</b>
        </div>
      );
    }
  };

  // instantiateContract() {
  //   /*
  //    * SMART CONTRACT EXAMPLE
  //    *
  //    * Normally these functions would be called in the context of a
  //    * state management library, but for convenience I've placed them here.
  //    */
  //
  //   const contract = require('truffle-contract')
  //   const simpleStorage = contract(SimpleStorageContract)
  //   simpleStorage.setProvider(this.state.web3.currentProvider)
  //
  //   // Declaring this for later so we can chain functions on SimpleStorage.
  //   var simpleStorageInstance
  //
  //   // Get accounts.
  //   this.state.web3.eth.getAccounts((error, accounts) => {
  //     simpleStorage.deployed().then((instance) => {
  //       simpleStorageInstance = instance
  //
  //       // Stores a given value, 5 by default.
  //
  //     return simpleStorageInstance.filesCount.call(accounts[0])
  //
  //     }).then((result) => {
  //        this.setState({ filesCount: result.c[0] })
  //       // Get the value from the contract to prove it worked.
  //       return simpleStorageInstance.get.call(accounts[0])
  //     }).then((result) => {
  //       // Update state with the result.
  //       return this.setState({ storageValue: result.c[0] })
  //     }).then( result => {
  //       return simpleStorageInstance.filesList.call(0)
  //     }).then( result => console.log(result[0]))
  //   })
  //
  //
  //
  //
  // }

  // anotherValue(){
  //   const contract = require('truffle-contract')
  //   const simpleStorage = contract(SimpleStorageContract)
  //   simpleStorage.setProvider(this.state.web3.currentProvider)
  //   var simpleStorageInstance
  //
  //   this.state.web3.eth.getAccounts((error, accounts) => {
  //     simpleStorage.deployed().then((instance) => {
  //       simpleStorageInstance = instance
  //
  //       // Stores a given value, 5 by default.
  //       return simpleStorageInstance.set(this.state.storageValue + 10, {from: accounts[0]})
  //     }).then((result) => {
  //       // Get the value from the contract to prove it worked.
  //       return simpleStorageInstance.get.call(accounts[0])
  //     }).then((result) => {
  //       // Update state with the result.
  //       return this.setState({ storageValue: result.c[0] })
  //     })
  //   })
  // }

  startCalculation() {
    fetch(
      `https://ipfs.io/ipfs/${
        this.state.ipfsFilesArr[this.state.selectedDatasetIndex].ipfsHash
      }`
    )
      .then(data => data.json())
      .then(dataJson => {
        this.setState({
          prob: this.state.regression.calculate(
            dataJson,
            this.state.firstValutation,
            this.state.secondValutation
          )
        });
      });
  }

  render() {
    const symptoms = [
      "Bad smell",
      "Red eyes",
      "Red nose",
      "Veins on the forehead",
      "Veins on the cheeks"
    ];

    const ipfsList = this.state.ipfsFilesArr.map((item, index) => (
      <label>
        <input
          type="radio"
          value={index}
          checked={this.state.selectedDatasetIndex === index}
          onChange={this.handleOptionChange}
        />
        <span className="radio-text">{item.ipfsHash}</span>
        <a href={`https://ipfs.io/ipfs/${ this.state.ipfsFilesArr[index].ipfsHash }`} target="_blank">
        [GO]
        </a>
      </label>
    ));

    const ptCheckboxes = val =>
      symptoms.map((item, index) => (
        <div>
          <label>
            <input type="checkbox" onClick={this.handleChecking(val)} />
          </label>
          {item}
        </div>
      ));





    return (
      <div className="App">
        <header className="App-header">
          <div className="App-conf-container">
            <div className="App-conf blur_anim">CONFIDENTIAL</div>
          </div>
        </header>
        <h1 className="App-title">Zombification Prediction Program</h1>
        <h1 className="App-sub-title">NanaDead Corporation</h1>
        <div className="App-intro-container padding-cont">
          <div className="App-intro">
            Welcome to the Zombification Prediction application made by <a href="https://www.linkedin.com/in/lorenzo-zaccagnini/" target="_blank">Lorenzo Zaccagnini</a> for the NanaDead Corporation.
            <br />
            This application uses Ethereum blockchain, IPFS, and AI Machine Learning, requires Metamask logged to the Ethereum Rinkeby testnet in order to work
            <br />
            The datasets are stored in IPFS, the Ethereum blockchain smart contract contains the links pointing to IPFS. The application gets the links from the smart contract and then loads the dataset from IPFS, the data from IPFS is analyzed using the logistic regression machine learning algorithm allowing us to make predictions on new data.
            <p>The AI name is Alicia "The Red Queen"</p>
            {this.checkNet()}
            <div className="App-bordered-section">
              <h1>DESCRIPTION</h1>
              We are injecting two "safe" versions of the zombie virus, ZMB1 and ZMB2. Check the volunteer symptoms and ALICIA will tell you if the patient is immune to the zombie virus. If the patient is immune contact us immediately, we are developing a vaccine.
            </div>
            <div className="App-bordered-section">
            DATASETS FROM IPFS [SEL: {this.state.selectedDatasetIndex + 1}]
            <form>{ipfsList}</form>
            </div>
          </div>
        </div>

        <main className="container padding-cont">
        <h2>CHECK THE PATIENT SYMPTOMS</h2>
          <div className="patient-card-container">
            <div className="patient-card">
              FIRST VALUTATION
              <div>
                <img
                  src={require(`./imgs/pt_${
                    this.state.firstValutationImg
                  }.png`)}
                  alt=""
                />
              </div>
              <div className="checkboxes-container">{ptCheckboxes(1)}</div>
              POINTS: {this.state.firstValutation}
            </div>
            <div className="patient-card">
              SECOND VALUTATION
              <div>
                <img
                  src={require(`./imgs/pt_${
                    this.state.secondValutationImg
                  }.png`)}
                  alt=""
                />
              </div>
              <div className="checkboxes-container">{ptCheckboxes(2)}</div>
              POINTS: {this.state.secondValutation}
            </div>

          </div>
          <button className="btn-green" onClick={this.startCalculation}>CALCULATE</button>
          <div className="App-outcome">
            <Outcome prob={this.state.prob} />
          </div>
          <div className="App-bordered-section">
          <h1>{this.state.authorized}</h1>
            <h1>Upload your dataset</h1>
            <p>
              Only members can upload datasets, contact us if you
              want to be a member
            </p>
            <h2>Upload Dataset</h2>
            <form onSubmit={this.onSubmit}>
              <input type="file" onChange={this.captureFile} />
              <br />
              <br />
              <br />
              <input className="btn-green" type="submit" value="UPLOAD" />
            </form>
          </div>
        </main>
      </div>
    );
  }
}

export default App;
