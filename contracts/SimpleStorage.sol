pragma solidity ^0.4.18;

contract SimpleStorage {
  uint storedData;
  uint public filesCount;

  struct FileSchema {
      string ipfsHash;
      uint timestamp;
  }

  FileSchema[] public filesList;

  function set(uint x) public {
    storedData = x;
  }

  function addFile(string _ipfsHash) public {
      filesList.push(FileSchema(_ipfsHash, now));
      filesCount++;
  }


  function get() public view returns (uint) {
    return storedData;
  }
}
