pragma solidity ^ 0.4.18;
contract ZombieIPFS {

    uint public totalDocuments;
    address owner;

    mapping(address => bool) public ableToAdd;

    constructor() {
        owner = msg.sender;
        ableToAdd[owner] = true;
    }

    struct ipfsCsvHash {
        string ipfsHash;
        uint insertedAt;
    }

    ipfsCsvHash[] public ipfsCsvHashes;

    function checkMembership() public view returns(bool _isAble) {
        return ableToAdd[msg.sender];
    }

    function addNewDocument(string _ipfsHash)
    isAbleToAdd()
    public {
        ipfsCsvHashes.push(ipfsCsvHash(_ipfsHash, now));
        totalDocuments++;
    }

    function manageMember(address _memberAddress, bool _enableAdd)
    isOwner()
    public {
        ableToAdd[_memberAddress] = _enableAdd;
    }

    modifier isAbleToAdd() {
        require(ableToAdd[msg.sender] == true);
        _;
    }

    modifier isOwner() {
        require(msg.sender == owner);
        _;
    }
}